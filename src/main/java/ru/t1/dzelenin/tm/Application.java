package ru.t1.dzelenin.tm;

import ru.t1.dzelenin.tm.constant.ArgumentConst;

import ru.t1.dzelenin.tm.constant.CommandConst;

import java.util.Scanner;

import static ru.t1.dzelenin.tm.constant.CommandConst.*;

public final class Application {

    public static void main(String[] args) {
        processArguments(args);
        System.out.println("** WELCOME **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
        System.exit(0);
    }

    public static void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
                break;
        }
    }

    private static void exit() {
        System.exit(0);
    }

    public static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
        System.exit(1);
    }

    public static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
        System.exit(2);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Daniil Zelenin");
        System.out.println("email: dzelenin@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s : Show developer info.\n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s : Show application version.\n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s : Show command list.\n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s : Exit application.\n", CommandConst.EXIT);
    }

}
